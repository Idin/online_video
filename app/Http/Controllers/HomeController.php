<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function movieList()
    {
        $output = view('playlist');

        $output .= "#EXT-X-MEDIA-SEQUENCE:" . floor(time() / 10) . "\n";
        $firstClip = floor(date("s") / 10 + 1);
        $secondClip = $firstClip + 1 < 7 ? $firstClip + 1 : 1;

        $output .= "#EXTINF:10,\n/movies/{$firstClip}.ts\n";
        $output .= "#EXTINF:10,\n/movies/{$secondClip}.ts\n";

        if ($secondClip == 6) {
             $output .= "#EXT-X-ENDLIST\n";
        }

        return response($output)
            ->header('Content-Type', 'application/vnd.apple.mpegurl');
    }
}
