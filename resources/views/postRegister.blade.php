@extends('layouts.app')

@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="home-title">
            <h2>Registeration was successfull</h2>
			<p>Please verify your email to complete the registeration</p>
        </div>
    </div>
</div>
@endsection
