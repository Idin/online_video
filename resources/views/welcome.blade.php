@extends('layouts.app')

@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="home-title">
            <h2>Online video website</h2>
            <p>For watching videos, please Login in site</p>
        </div>
    </div>
</div>
@endsection
