@extends('layouts.app')

@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="home-title">
            <h2>Email Verification</h2>
			@if ($user)
				<p>Your Email Verified Successfully. You can login now.</p>
			@else
				<p>Your verification code is not valid</p>
			@endif
        </div>
    </div>
</div>
@endsection
