@extends('layouts.app')

@section('content')

<link href="{{ asset('plugin/flowplayer-7.2.6/skin/skin.css') }}" rel="stylesheet">
<script src="{{ asset('plugin/flowplayer-7.2.6/hls.light.min.js') }}"></script>
<script src="{{ asset('plugin/flowplayer-7.2.6/flowplayer.min.js') }}"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Show Video</div>

                <div class="card-body">

                    <!-- the player -->
                    <div class="flowplayer" data-live="true" data-autoplay="true" data-share="false">
                        <video data-title="Live stream">
                            <source type="application/x-mpegurl" src="{{ asset('movieList') }}.m3u8"></source>
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

flowplayer(function (api) {
    api.on("finish", function (e, api) {
        api.load([
          { mpegurl: "{{ asset('movieList') }}.m3u8" }
        ]);
    });
});

</script>

@endsection
